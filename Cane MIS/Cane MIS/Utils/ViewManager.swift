//
//  ViewManager.swift
//  Cane MIS
//
//  Created by Pawan Ramteke on 13/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import SKActivityIndicatorView
import SVProgressHUD
import KeychainSwift

class ViewManager
{
    static let shared = ViewManager()
    var seasonID : String!
    var crushingStartDate : String!
    var crushingEndDate : String!

    private init(){
        
    }
    
    func showActivityIndicator()
    {
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setRingThickness(2)
        SVProgressHUD.show()
        //SKActivityIndicator.show("", userInteractionStatus: false)
    }

    func hideActivityIndicator()
    {
        SVProgressHUD.dismiss()
       // SKActivityIndicator.dismiss()
    }
    
    func getMISSeasonID(completion: @escaping (Bool) -> Void)
    {
        self.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_MIS_SESSION_ID_API, params: [:], completion: { (response) in
            self.hideActivityIndicator()
            let seasonId = response as! String
            if seasonId.count > 0
            {
                self.seasonID = seasonId
                completion(true)
            }
            else{
                completion(false)
            }
        }) { (errMsg) in
            self.hideActivityIndicator()
            completion(false)

        }
    }
    
    func getDeviceID() ->String
    {
        let keychain = KeychainSwift()
        var keychainId = keychain.get("UniqueId")
        if  keychainId == nil {
            let deviceId = UIDevice.current.identifierForVendor!.uuidString
            keychain.set(deviceId, forKey: "UniqueId")
        }
        
        keychainId = keychain.get("UniqueId")
        return keychainId!
    }
}
