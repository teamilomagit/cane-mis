//
//  UIColor+App.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
    public class var appThemeColor: UIColor
    {
        return UIColor(red: 0.19, green: 0.54, blue: 0.81, alpha: 1.0)
    }
    public class var navigationStripeColor: UIColor
    {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
    }
    public class var controllerBGColor : UIColor
    {
        return UIColor(red: 0.94, green: 0.94, blue: 0.94, alpha: 1.0)

    }
}
