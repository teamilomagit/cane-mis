//
//  Preferences.swift
//  Cane MIS
//
//  Created by Pawan Ramteke on 13/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import UIKit
let USER_DEFAULTS  = UserDefaults.standard
class Preferences
{
    class func saveUserId(userId:String)
    {
        USER_DEFAULTS.setValue(userId, forKey: "saveUserId")
    }
    
    class func getUserId() -> String?
    {
        return USER_DEFAULTS.value(forKey: "saveUserId") as? String
    }
    
    class func saveUserMobileNo(userId:String)
    {
        USER_DEFAULTS.setValue(userId, forKey: "saveUserMobileNo")
    }
    
    class func getUserMobileNo() -> String?
    {
        return USER_DEFAULTS.value(forKey: "saveUserMobileNo") as? String
    }
    
    class func setIMEIShown(status:Bool)
    {
        USER_DEFAULTS.set(status, forKey: "setIMEIShown")
    }
    
    class func isIMEIShown() -> Bool?
    {
        let status = USER_DEFAULTS.bool(forKey: "setIMEIShown")
        return status
    }
    
    class func saveIPAddress(IPAddress:String) {
        USER_DEFAULTS.setValue(IPAddress, forKey: "saveIPAddress")
    }
    
    class func getIPAddress() -> String? {
        return USER_DEFAULTS.value(forKey: "saveIPAddress") as? String
    }
    
    class func saveCompanyName(companyName:String) {
        USER_DEFAULTS.setValue(companyName, forKey: "saveCompanyName")
    }
    
    class func getCompanyName() -> String? {
        return USER_DEFAULTS.value(forKey: "saveCompanyName") as? String
    }
}

