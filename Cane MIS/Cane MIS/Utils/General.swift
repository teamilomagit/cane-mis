//
//  General.swift
//  Cane MIS
//
//  Created by Pawan Ramteke on 13/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation

let VIEWMANAGER = ViewManager.shared


//ISMCL BASE URL
//let BASE_URL = "http://117.206.85.194:80/WebServices/CaneMIS/"

let BASE_URL = "http://" + Preferences.getIPAddress()! + "/WebServices/CaneMIS/"

//JBSSK BASE URL
//let BASE_URL = "http://59.90.45.82:3389/WebServices/CaneMIS/"

let LOGIN_API                       = "MIS_Mobile_Login.php"

let GET_SHIFTWISE_DATA_API          = "MIS_ShiftWise.php"
let GET_VEHICLE_DATA_API            = "MIS_CaneYardVehicleBalance.php"
let GET_MIS_SESSION_ID_API          = "MIS_Seasion_ID.php"
let GET_CRUSHING_START_END_DATE_API = "MIS_Crushing_Start_End_Date.php"
let GET_MIS_GUTWISE_API             = "MIS_Gutwise_Tonnage.php"
let GET_MIS_VILLAGEWISE_API         = "MIS_Villagewise_Tonnage.php"
let GET_MIS_SHIFTWISE_API           = "MIS_Cane_Shift_Details.php"
