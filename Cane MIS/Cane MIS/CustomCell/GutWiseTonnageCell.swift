//
//  GutWiseTonnageCell.swift
//  Cane MIS
//
//  Created by TIU-User on 02/12/20.
//  Copyright © 2020 iLoma. All rights reserved.
//

import UIKit

class GutWiseTonnageCell: UITableViewCell {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblGutName: UILabel!
    @IBOutlet weak var lblTodayTonnage: UILabel!
    @IBOutlet weak var lblUptoDateTonnage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
