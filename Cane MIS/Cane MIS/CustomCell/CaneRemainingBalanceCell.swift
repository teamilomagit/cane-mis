//
//  CaneRemainingBalanceCell.swift
//  Cane MIS
//
//  Created by iLoma on 13/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import UIKit

class CaneRemainingBalanceCell: UITableViewCell {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblVehicleData: UILabel!
    @IBOutlet weak var lblTonnageData: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
