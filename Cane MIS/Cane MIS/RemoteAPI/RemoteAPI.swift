//
//  RemoteAPI.swift
//  BTS
//
//  Created by Pawan Ramteke on 22/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class RemoteAPI{
    
    let sessionManager = Alamofire.Session.default

    func callPOSTApi(apiName:String,params:[String: AnyObject],completion: @escaping (AnyObject) -> Void,failed:@escaping (_ errorMsg:String?) -> Void)
    {
        let fullAPI = "\(BASE_URL)\(apiName)"
        sessionManager.request(fullAPI, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { response in
            
                debugPrint(response)
                if let ERR = response.error
                {
                    print(ERR);
                    failed("Enter valid number registered on ERP")
                    return
                }
                
                if let JSON = response.value
                {
                    let serverResponse = JSON as? NSDictionary
                    if serverResponse?.value(forKey: "status") as! String == "fail"
                    {
                        failed(serverResponse?.value(forKey: "msg") as? String)
                        return
                    }
                    
                    let obj = ModelParser().modelParserWithData(apiURL: apiName, responseData: serverResponse?.value(forKey: "response") as AnyObject)
                    completion(obj)
                }
        }
    }
}

