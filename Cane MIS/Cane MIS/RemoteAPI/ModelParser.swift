//
//  ModelParser.swift
//  BTS
//
//  Created by Pawan Ramteke on 22/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit
class ModelParser {
    
    func modelParserWithData(apiURL:String,responseData:AnyObject)->AnyObject
    {
        if apiURL == LOGIN_API  {
            return parseLoginData(responseData: responseData) as AnyObject
        }
        
        if apiURL == GET_SHIFTWISE_DATA_API {
           return parseShiftWiseData(responseData: responseData as! NSArray)
        }
        
        if apiURL == GET_VEHICLE_DATA_API {
            return parseVehicleData(responseData: responseData as! NSArray)
        }
        
        if apiURL == GET_MIS_SESSION_ID_API {
            return parseMISSeasonIDData(resposeData: responseData)
        }
        
        if apiURL == GET_CRUSHING_START_END_DATE_API {
            return parseCrushingStartEndDateData(resposeData: responseData)
        }
        
        if apiURL == GET_MIS_GUTWISE_API  || apiURL == GET_MIS_VILLAGEWISE_API  || apiURL == GET_MIS_SHIFTWISE_API {
            return parseGutWiseTonnageData(responseData: responseData as! NSArray)
        }
        return [] as AnyObject
    }
    
    
    func parseLoginData(responseData:AnyObject) -> AnyObject
    {
        if responseData is NSArray
        {
            let data = responseData as! NSArray
            if data.count > 0
            {
                let dict = data[0] as! NSDictionary
                Preferences.saveUserId(userId: dict["USER_ID"] as! String)
                return true as AnyObject
            }
        }
        return false as AnyObject
    }
    
    func parseShiftWiseData(responseData : NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = ShiftWiseModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    func parseVehicleData(responseData : NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = VehicleDataModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseMISSeasonIDData(resposeData:AnyObject) -> AnyObject
    {
        if resposeData is NSArray
        {
            let data = resposeData as! NSArray
            if data.count > 0
            {
                let dict = data[0] as! NSDictionary
                
                return dict["SEASON_ID"] as AnyObject
            }
        }
        return "" as AnyObject
    }
   
    
    func parseCrushingStartEndDateData(resposeData:AnyObject) -> AnyObject
    {
        if resposeData is NSArray
        {
            let data = resposeData as! NSArray
            if data.count > 0
            {
                let dict = data[0] as! NSDictionary
                VIEWMANAGER.crushingStartDate = dict.value(forKey: "START_DATE") as? String
                VIEWMANAGER.crushingEndDate = dict.value(forKey: "END_DATE") as? String
                return true as AnyObject
            }
        }
        return false as AnyObject
    }

    
    func parseGutWiseTonnageData(responseData : NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = GutWiseTonnageModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    
    
    
    
}
