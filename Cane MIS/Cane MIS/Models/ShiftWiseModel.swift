//
//  ShiftWiseModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 13, 2018

import Foundation


class ShiftWiseModel : NSObject, NSCoding{

    var aLCOHOLNETWT : String!
    var aSALCOHOLNETWT : String!
    var aSCOGENNETWT : String!
    var aSCRUSNETWT : String!
    var aSMOLANETWT : String!
    var aSSUGARNETWT : String!
    var cOGENNETWT : String!
    var cRUSNETWT : String!
    var mOLANETWT : String!
    var sHIFTCODE : String!
    var sUGARNETWT : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        aLCOHOLNETWT = dictionary["ALCOHOL_NET_WT"] as? String
        aSALCOHOLNETWT = dictionary["AS_ALCOHOL_NET_WT"] as? String
        aSCOGENNETWT = dictionary["AS_COGEN_NET_WT"] as? String
        aSCRUSNETWT = dictionary["AS_CRUS_NET_WT"] as? String
        aSMOLANETWT = dictionary["AS_MOLA_NET_WT"] as? String
        aSSUGARNETWT = dictionary["AS_SUGAR_NET_WT"] as? String
        cOGENNETWT = dictionary["COGEN_NET_WT"] as? String
        cRUSNETWT = dictionary["CRUS_NET_WT"] as? String
        mOLANETWT = dictionary["MOLA_NET_WT"] as? String
        sHIFTCODE = dictionary["SHIFT_CODE"] as? String
        sUGARNETWT = dictionary["SUGAR_NET_WT"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if aLCOHOLNETWT != nil{
            dictionary["ALCOHOL_NET_WT"] = aLCOHOLNETWT
        }
        if aSALCOHOLNETWT != nil{
            dictionary["AS_ALCOHOL_NET_WT"] = aSALCOHOLNETWT
        }
        if aSCOGENNETWT != nil{
            dictionary["AS_COGEN_NET_WT"] = aSCOGENNETWT
        }
        if aSCRUSNETWT != nil{
            dictionary["AS_CRUS_NET_WT"] = aSCRUSNETWT
        }
        if aSMOLANETWT != nil{
            dictionary["AS_MOLA_NET_WT"] = aSMOLANETWT
        }
        if aSSUGARNETWT != nil{
            dictionary["AS_SUGAR_NET_WT"] = aSSUGARNETWT
        }
        if cOGENNETWT != nil{
            dictionary["COGEN_NET_WT"] = cOGENNETWT
        }
        if cRUSNETWT != nil{
            dictionary["CRUS_NET_WT"] = cRUSNETWT
        }
        if mOLANETWT != nil{
            dictionary["MOLA_NET_WT"] = mOLANETWT
        }
        if sHIFTCODE != nil{
            dictionary["SHIFT_CODE"] = sHIFTCODE
        }
        if sUGARNETWT != nil{
            dictionary["SUGAR_NET_WT"] = sUGARNETWT
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        aLCOHOLNETWT = aDecoder.decodeObject(forKey: "ALCOHOL_NET_WT") as? String
        aSALCOHOLNETWT = aDecoder.decodeObject(forKey: "AS_ALCOHOL_NET_WT") as? String
        aSCOGENNETWT = aDecoder.decodeObject(forKey: "AS_COGEN_NET_WT") as? String
        aSCRUSNETWT = aDecoder.decodeObject(forKey: "AS_CRUS_NET_WT") as? String
        aSMOLANETWT = aDecoder.decodeObject(forKey: "AS_MOLA_NET_WT") as? String
        aSSUGARNETWT = aDecoder.decodeObject(forKey: "AS_SUGAR_NET_WT") as? String
        cOGENNETWT = aDecoder.decodeObject(forKey: "COGEN_NET_WT") as? String
        cRUSNETWT = aDecoder.decodeObject(forKey: "CRUS_NET_WT") as? String
        mOLANETWT = aDecoder.decodeObject(forKey: "MOLA_NET_WT") as? String
        sHIFTCODE = aDecoder.decodeObject(forKey: "SHIFT_CODE") as? String
        sUGARNETWT = aDecoder.decodeObject(forKey: "SUGAR_NET_WT") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if aLCOHOLNETWT != nil{
            aCoder.encode(aLCOHOLNETWT, forKey: "ALCOHOL_NET_WT")
        }
        if aSALCOHOLNETWT != nil{
            aCoder.encode(aSALCOHOLNETWT, forKey: "AS_ALCOHOL_NET_WT")
        }
        if aSCOGENNETWT != nil{
            aCoder.encode(aSCOGENNETWT, forKey: "AS_COGEN_NET_WT")
        }
        if aSCRUSNETWT != nil{
            aCoder.encode(aSCRUSNETWT, forKey: "AS_CRUS_NET_WT")
        }
        if aSMOLANETWT != nil{
            aCoder.encode(aSMOLANETWT, forKey: "AS_MOLA_NET_WT")
        }
        if aSSUGARNETWT != nil{
            aCoder.encode(aSSUGARNETWT, forKey: "AS_SUGAR_NET_WT")
        }
        if cOGENNETWT != nil{
            aCoder.encode(cOGENNETWT, forKey: "COGEN_NET_WT")
        }
        if cRUSNETWT != nil{
            aCoder.encode(cRUSNETWT, forKey: "CRUS_NET_WT")
        }
        if mOLANETWT != nil{
            aCoder.encode(mOLANETWT, forKey: "MOLA_NET_WT")
        }
        if sHIFTCODE != nil{
            aCoder.encode(sHIFTCODE, forKey: "SHIFT_CODE")
        }
        if sUGARNETWT != nil{
            aCoder.encode(sUGARNETWT, forKey: "SUGAR_NET_WT")
        }
    }
}