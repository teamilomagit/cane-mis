//
//  GutWiseTonnageModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 3, 2020

import Foundation


class GutWiseTonnageModel : NSObject {

    var aSONTONNAGE : String!
    var cODE : String!
    var nAME : String!
    var tODAYTONNAGE : String!
    
    
    var cUMULATIVEWT : String!
    var fROMTOTIME : String!
    var hTIME : String!
    var nETWT : String!
    var oUTDATE : String!
    var oUTSHIFTCODE : String!

    
    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        aSONTONNAGE = dictionary["ASON_TONNAGE"] as? String
        cODE = dictionary["CODE"] as? String
        nAME = dictionary["NAME"] as? String
        tODAYTONNAGE = dictionary["TODAY_TONNAGE"] as? String
        
        cUMULATIVEWT = dictionary["CUMULATIVE_WT"] as? String
        fROMTOTIME = dictionary["FROM_TO_TIME"] as? String
        hTIME = dictionary["H_TIME"] as? String
        nETWT = dictionary["NET_WT"] as? String
        oUTDATE = dictionary["OUT_DATE"] as? String
        oUTSHIFTCODE = dictionary["OUT_SHIFT_CODE"] as? String
    }

}
