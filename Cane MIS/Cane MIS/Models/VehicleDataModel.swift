//
//  VehicleDataModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 14, 2018

import Foundation


class VehicleDataModel : NSObject, NSCoding{

    var vNAME : String!
    var pENDINGSLIP : String!
    var sUMQTY : String!
    var tOtal : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        vNAME = dictionary["V_NAME"] as? String
        pENDINGSLIP = dictionary["PENDING_SLIP"] as? String
        sUMQTY = dictionary["SUM_QTY"] as? String
    }

    override init() {
        
    }
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if vNAME != nil{
            dictionary["V_NAME"] = vNAME
        }
        if pENDINGSLIP != nil{
            dictionary["PENDING_SLIP"] = pENDINGSLIP
        }
        if sUMQTY != nil{
            dictionary["SUM_QTY"] = sUMQTY
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        vNAME = aDecoder.decodeObject(forKey: "V_NAME") as? String
        pENDINGSLIP = aDecoder.decodeObject(forKey: "PENDING_SLIP") as? String
        sUMQTY = aDecoder.decodeObject(forKey: "SUM_QTY") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if vNAME != nil{
            aCoder.encode(vNAME, forKey: "V_NAME")
        }
        if pENDINGSLIP != nil{
            aCoder.encode(pENDINGSLIP, forKey: "PENDING_SLIP")
        }
        if sUMQTY != nil{
            aCoder.encode(sUMQTY, forKey: "SUM_QTY")
        }
    }
}
