//
//  CustomAlertView.swift
//  Cane MIS
//
//  Created by Pawan Ramteke on 13/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import UIKit

class CustomAlertView: UIView {
    var baseView : UIView!
    init(frame: CGRect, strMessage: String) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.alpha = 0
        
        baseView = UIView(frame: CGRect(x: 10, y: self.frame.midY - 100/2, width: self.frame.size.width - 20, height: 100))
        baseView.layer.cornerRadius = 5;
        baseView.backgroundColor = .white
        self.addSubview(baseView)
        
        let lblMessage = UILabel(frame: CGRect(x: 20, y: 20, width: baseView.frame.size.width - 40, height: 100))
        lblMessage.font = UIFont.systemFont(ofSize: 16)
        lblMessage.text = strMessage
        lblMessage.textColor = UIColor.gray
        lblMessage.numberOfLines = 0
        lblMessage.sizeToFit()
        baseView.addSubview(lblMessage)
    
        let btnOK = UIButton(frame: CGRect(x: 10, y: lblMessage.frame.maxY + 10, width: baseView.frame.size.width - 20, height: 40))
        btnOK.backgroundColor = UIColor.appThemeColor
        btnOK.setTitle("OK", for: .normal)
        btnOK.layer.cornerRadius = 5;
        btnOK.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnOK.addTarget(self, action: #selector(CustomAlertView.btnCloseClicked), for: .touchUpInside)

        baseView.addSubview(btnOK)
        
        baseView.frame.size.height = btnOK.frame.maxY + 10
        baseView.frame.origin.y = self.frame.midY - baseView.frame.size.height/2
        UIView.animate(withDuration: 0.2) {
            self.alpha = 1;
        }
    }
    
    @objc func btnCloseClicked()
    {
        Preferences.setIMEIShown(status: true)
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }) { (completion) in
            self.removeFromSuperview()
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
