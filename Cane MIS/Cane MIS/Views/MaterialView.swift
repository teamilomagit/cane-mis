//
//  MaterialView.swift
//  Cane MIS
//
//  Created by Pawan Ramteke on 13/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import UIKit

class MaterialView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup()
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.2
    }
}
