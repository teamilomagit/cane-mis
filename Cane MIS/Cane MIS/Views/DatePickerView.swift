//
//  DatePickerView.swift
//  BTS
//
//  Created by Pawan Ramteke on 01/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class DatePickerView: UIView {
    
    var doneClosure : ((String)->())?

    var baseView : UIView!
    
    var datePicker : UIDatePicker!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.alpha = 0;
        
        baseView = UIView(frame: CGRect(x: 0, y: self.frame.size.height, width: self.frame.size.width, height: 300))
        baseView.backgroundColor = .white
        self.addSubview(baseView)
        
        datePicker = UIDatePicker(frame:CGRect(x: 0, y: 20, width: baseView.frame.size.width, height: 200))
        datePicker.backgroundColor = UIColor.white
        datePicker.datePickerMode = .date
        baseView.addSubview(datePicker)
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }

        if VIEWMANAGER.crushingEndDate != nil && VIEWMANAGER.crushingStartDate != nil {
            datePicker.maximumDate = maxDate()
            datePicker.minimumDate = minDate()
        }
        
        datePicker.enableAutoLayout()
        datePicker.leadingMargin(pixels: 0)
        datePicker.trailingMargin(pixels: 0)
        datePicker.topMargin(pixels: 20)
        datePicker.fixedHeight(pixels: 200)
        
        let btnCancel = UIButton(frame: CGRect(x: 10, y:datePicker.frame.maxY + 10, width:  baseView.frame.size.width/2 - 15, height: 40))
        btnCancel.backgroundColor = .red
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.setTitleColor(.white, for: .normal)
        btnCancel.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btnCancel.addTarget(self, action: #selector(DatePickerView.btnCancelClicked(sender:)), for: .touchUpInside)
        baseView.addSubview(btnCancel)
       
        let btnDone = UIButton(frame: CGRect(x: btnCancel.frame.maxX + 10, y:btnCancel.frame.minY, width: btnCancel.frame.size.width, height: 40))
        btnDone.backgroundColor = UIColor.appThemeColor
        btnDone.setTitle("Done", for: .normal)
        btnDone.setTitleColor(.white, for: .normal)
        btnDone.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btnDone.addTarget(self, action: #selector(DatePickerView.btnDoneClicked(sender:)), for: .touchUpInside)
        baseView.addSubview(btnDone)
        
        UIView.animate(withDuration: 0.2) {
            self.alpha = 1;
            self.baseView.frame.origin.y = self.frame.size.height - self.baseView.frame.size.height
        }
    }
    
    @objc func btnDoneClicked(sender:UIButton)
    {
        let selDate: Date? = datePicker.date
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MMM-yyyy"
        var dateString: String? = nil
        if let aDate = selDate {
            dateString = dateFormat.string(from: aDate)
        }
        
        if doneClosure != nil {
            doneClosure!(dateString!)
        }
        
        hideView()
    }
    
    @objc func btnCancelClicked(sender:UIButton)
    {
       hideView()
    }
    
    
    
    func onDoneClicked(back : @escaping (String) -> Void)
    {
        doneClosure = back
    }
    
    func hideView()
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0;
            self.baseView.frame.origin.y = self.frame.size.height
        }) { (completion) in
            self.removeFromSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    func maxDate()->Date
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yy"
        let date = formatter.date(from: VIEWMANAGER.crushingEndDate)
        return date!
    }
    
    func minDate()->Date
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yy"
        let date = formatter.date(from: VIEWMANAGER.crushingStartDate)
        return date!
    }
}
