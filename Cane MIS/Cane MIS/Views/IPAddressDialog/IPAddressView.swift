//
//  IPAddressView.swift
//  Cane MIS
//
//  Created by iLoma Technology on 19/12/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

class IPAddressView: UIView {

    @IBOutlet weak var btnBG: UIButton!
    @IBOutlet weak var BgView: UIView!
    @IBOutlet weak var txtFieldIPAddress: UITextField!
    @IBOutlet weak var txtFieldCompanyName: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    var successBlk : (()->())?
    
    var isDismissable : Bool! {
        didSet {
            btnBG.isUserInteractionEnabled = isDismissable
        }
    }
    override func awakeFromNib() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        btnSave.addTarget(self, action: #selector(saveView), for: .touchUpInside)
        
        self.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.alpha = 1
        }
    }

    @objc func saveView(){
        if validate() {
            Preferences.saveIPAddress(IPAddress: txtFieldIPAddress.text!)
            Preferences.saveCompanyName(companyName: txtFieldCompanyName.text!)
            hideView()
            successBlk?()
        }
    }
    
    func validate() -> Bool {
        if txtFieldIPAddress.text!.count == 0 {
            self.makeToast("Please enter IP Address")
            return false
        }
        if txtFieldCompanyName.text!.count == 0 {
            self.makeToast("Please enter company name")
            return false
        }
        return true
    }
    
    func hideView() {
        UIView.animate(withDuration: 0.4) {
            self.alpha = 0
        } completion: { (_) in
            self.removeFromSuperview()
        }
    }
    
    func onSuccess(closure:@escaping ()->()) {
        successBlk = closure
    }
    
    @IBAction func btnGBClicked(_ sender: Any) {
        hideView()
    }
    
}

