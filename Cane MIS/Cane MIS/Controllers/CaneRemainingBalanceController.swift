//
//  CaneRemainingBalanceController.swift
//  Cane MIS
//
//  Created by iLoma on 13/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import UIKit

class CaneRemainingBalanceController: UIViewController,UITableViewDelegate,UITableViewDataSource {
@IBOutlet weak var viewDateTime: UIView!
@IBOutlet weak var lblDate: UILabel!
    var timer = Timer()
    var arrTotal = NSMutableArray()
    var arrData = NSMutableArray()
    var selDate : String!
    
@IBOutlet weak var tableViewRemainingBalance: UITableView!

   ///let arrHeader = ["Types Of Vehicle", "Numbers Of Vehicle", "Tonnage (Approx)"]

    var array1 = ["  CART","  MINI TRACTOR","  TRACTOR","  Total"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewRemainingBalance.delegate = self
        tableViewRemainingBalance.dataSource = self
        self.setDateAndTime()
        
        selDate = getTodaysDate()
        if VIEWMANAGER.seasonID == nil {
            VIEWMANAGER.getMISSeasonID(completion: { (status) in
                if status
                {
                    self.getVehicleData(date: self.selDate,season: VIEWMANAGER.seasonID)
                }
            })
        }
        else{
            self.getVehicleData(date: self.selDate,season: VIEWMANAGER.seasonID)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10)) {
            self.getVehicleData(date: self.selDate,season: VIEWMANAGER.seasonID != nil ? VIEWMANAGER.seasonID : "",showLoader: false)
        }

    }
    
    func getVehicleData(date:String,season:String,showLoader:Bool = true)
    {
        let param = [
            "date":date,
            "season":8
            ] as [String : AnyObject]
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        RemoteAPI().callPOSTApi(apiName: GET_VEHICLE_DATA_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrData = responseData as! NSMutableArray
            var totalNoOfVehicle : Float = 0
            var totalTonnag : Float = 0
            for mod in self.arrData
            {
                let model = mod as! VehicleDataModel
                totalNoOfVehicle = totalNoOfVehicle + Float(model.pENDINGSLIP)!
                totalTonnag = totalTonnag + Float(model.sUMQTY)!
            }
            
            let model = VehicleDataModel()
            model.vNAME = "Total"
            model.pENDINGSLIP = "\(totalNoOfVehicle)"
            model.sUMQTY = "\(totalTonnag)"
            self.arrData.add(model)
            self.tableViewRemainingBalance.reloadData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
             self.view.makeToast(errMsg)
        }
    }
    
    func setDateAndTime() {
        
        lblDate.text = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .medium)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
    }
    
    @objc func tick() {
        lblDate.text = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .medium)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  arrData.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let  cell:CaneRemainingBalanceCell = tableView.dequeueReusableCell(withIdentifier: "cellBalance") as! CaneRemainingBalanceCell
        cell.lblTitle.font = UIFont.boldSystemFont(ofSize: 14)

        let model = arrData[indexPath.row] as! VehicleDataModel
        cell.lblTitle.text = " \(model.vNAME!)"
        cell.lblVehicleData.text = model.pENDINGSLIP
        cell.lblTonnageData.text = model.sUMQTY
        
        cell.lblTitle.textAlignment = .left
        cell.lblVehicleData.textAlignment = .right
        cell.lblTonnageData.textAlignment = .right
        
        cell.lblTitle.font = UIFont.boldSystemFont(ofSize: 15)
        cell.lblTonnageData.font = UIFont.systemFont(ofSize: 15)
        cell.lblTonnageData.font = UIFont.systemFont(ofSize: 15)
        
        if indexPath.row == arrData.count - 1 {
            cell.lblTitle.backgroundColor = UIColor.clear
            cell.baseView.backgroundColor = UIColor.darkGray
            cell.lblTitle.textColor = UIColor.white
            cell.lblVehicleData.textColor = UIColor.white
            cell.lblTonnageData.textColor = UIColor.white
        }
        else{
            cell.lblTitle.backgroundColor = UIColor.lightGray
            cell.baseView.backgroundColor = UIColor.controllerBGColor
            cell.lblTitle.textColor = UIColor.black
            cell.lblVehicleData.textColor = UIColor.black
            cell.lblTonnageData.textColor = UIColor.black
        }
        
    
       
        
        return cell
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        
        let baseView = UIView()
        baseView.backgroundColor = UIColor.lightGray
        headerView.addSubview(baseView)
        baseView.enableAutoLayout()
        baseView.leadingMargin(pixels: 5)
        baseView.topMargin(pixels: 0)
        baseView.bottomMargin(pixels: 0)
        baseView.trailingMargin(pixels: 5)
        
        let lblTitle1 = UILabel()
        lblTitle1.font = UIFont.boldSystemFont(ofSize: 15)
        lblTitle1.text = "Types of Vehicle"
        lblTitle1.numberOfLines = 0;
        lblTitle1.textAlignment = .left
        baseView.addSubview(lblTitle1)
        
        lblTitle1.enableAutoLayout()
        lblTitle1.leadingMargin(pixels: 2)
        lblTitle1.topMargin(pixels: 0)
        lblTitle1.bottomMargin(pixels: 0)
        
        let lblTitle2 = UILabel()
        lblTitle2.font = UIFont.boldSystemFont(ofSize: 15)
        lblTitle2.text = "Number of Vehicle"
        lblTitle2.numberOfLines = 0;
        lblTitle2.textAlignment = .right
        baseView.addSubview(lblTitle2)
        
        lblTitle2.enableAutoLayout()
        lblTitle2.addToRightToView(view: lblTitle1, pixels: 0)
        lblTitle2.topMargin(pixels: 0)
        lblTitle2.bottomMargin(pixels: 0)
        lblTitle2.equalWidthToView(toView: lblTitle1)
        
        let lblTitle3 = UILabel()
        lblTitle3.font = UIFont.boldSystemFont(ofSize: 15)
        lblTitle3.text = "Tonnage (Approx)"
        lblTitle3.numberOfLines = 0;
        lblTitle3.textAlignment = .right
        baseView.addSubview(lblTitle3)
        
        lblTitle3.enableAutoLayout()
        lblTitle3.addToRightToView(view: lblTitle2, pixels: 2)
        lblTitle3.topMargin(pixels: 0)
        lblTitle3.bottomMargin(pixels: 0)
        lblTitle3.trailingMargin(pixels: 2)
        lblTitle3.equalWidthToView(toView: lblTitle2)
        
        let separator1 = UIView()
        separator1.backgroundColor = .white
        baseView.addSubview(separator1)
        separator1.enableAutoLayout()
        separator1.addToRightToView(view: lblTitle1, pixels: 1)
        separator1.topMargin(pixels: 0)
        separator1.bottomMargin(pixels: 0)
        separator1.fixedWidth(pixels: 1)
        
        let separator2 = UIView()
        separator2.backgroundColor = .white
        baseView.addSubview(separator2)
        separator2.enableAutoLayout()
        separator2.addToRightToView(view: lblTitle2, pixels: 0)
        separator2.topMargin(pixels: 0)
        separator2.bottomMargin(pixels: 0)
        separator2.fixedWidth(pixels: 1)
        
        let separator = UIView()
        separator.backgroundColor = .white
        baseView.addSubview(separator)
        separator.enableAutoLayout()
        separator.addToRightToView(view: lblTitle2, pixels: 0)
        separator.bottomMargin(pixels: 0)
        separator.leadingMargin(pixels: 0)
        separator.trailingMargin(pixels: 0)
        separator.fixedHeight(pixels: 1)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }

    func getTodaysDate() -> String
    {
        let today: Date? = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MMM-yyyy"
        var dateString: String? = nil
        if let aDate = today {
            dateString = dateFormat.string(from: aDate)
        }
        return dateString!
    }
   
    @IBAction func btnHomeClicked(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
