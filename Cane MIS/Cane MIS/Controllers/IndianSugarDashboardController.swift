//
//  IndianSugarDashboardController.swift
//  Cane MIS
//
//  Created by iLoma on 11/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import UIKit
var startDate = String() as Any

class IndianSugarDashboardController: UIViewController {
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    var timer = Timer()
    @IBOutlet weak var viewDateTime: UIView!
    @IBOutlet weak var viewCaneCushingProduction: UIView!
    @IBOutlet weak var viewCaneYardBalance: UIView!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblCrushingDays: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        refreshUserLogin()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNavTitle.text = Preferences.getCompanyName()
        self.setDateAndTime()
        
        VIEWMANAGER.getMISSeasonID { (status) in
            if status {
                self.getMISCrushingStartEndDate(seasonID: VIEWMANAGER.seasonID)
            }
        }
        //let strDate = VIEWMANAGER.crushingStartDate
       
        
//       // let result = formatter.string(from: date)
        

       // let diff = startDate.timeIntervalSince(date)
//      ///lblCrushingDays.text = diff
//



    }
    

    func getMISCrushingStartEndDate(seasonID:String)
    {
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_CRUSHING_START_END_DATE_API, params: ["season":VIEWMANAGER.seasonID as AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yy"
            startDate = formatter.date(from: VIEWMANAGER.crushingStartDate)!
           let timeInterval = date.timeIntervalSince(startDate as! Date)
            let days = (Int(timeInterval)) / (60 * 60 * 24)
            self.lblStartDate.text = VIEWMANAGER.crushingStartDate

            self.lblCrushingDays.text = "\(days) \("Days")"


        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            self.view.makeToast(errMsg)
        }
    }
    
    @IBAction func btnCaneCrrushingProductionClicked(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let crushing = storyBoard.instantiateViewController(withIdentifier: "CaneCrushingController") as! CaneCrushingController
        self.navigationController?.pushViewController(crushing, animated: true)
    }
    

    @IBAction func btnCaneYardRemainingBalanceClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let balance = storyBoard.instantiateViewController(withIdentifier: "CaneRemainingBalanceController") as! CaneRemainingBalanceController
        self.navigationController?.pushViewController(balance, animated: true)
    }
    
    @IBAction func btnGutWiseTonnageClicked(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let balance = storyBoard.instantiateViewController(withIdentifier: "GutWiseTonnageController") as! GutWiseTonnageController
        self.navigationController?.pushViewController(balance, animated: true)

    }
    
    @IBAction func btnVillageWiseTonnageClicked(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let balance = storyBoard.instantiateViewController(withIdentifier: "GutWiseTonnageController") as! GutWiseTonnageController
        balance.fromVillageWise = true
        self.navigationController?.pushViewController(balance, animated: true)
    }
    
    
    @IBAction func btnShiftWiseTonnageClicked(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let balance = storyBoard.instantiateViewController(withIdentifier: "GutWiseTonnageController") as! GutWiseTonnageController
        balance.fromShiftWiseWise = true
        self.navigationController?.pushViewController(balance, animated: true)
    }
    
    
    func setDateAndTime() {
       
        lblDate.text = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .medium)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
    }
    
    @objc func tick() {
        lblDate.text = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .medium)
    }
    
    
    @IBAction func btnLogoutClicked(_ sender: Any) {
        showLogoutAlert()
    }
    
    
    @IBAction func btnSwitchCompanyClicked(_ sender: Any) {
        showIPAddressDialog()
    }
    
    func showIPAddressDialog(){
        let ipAddressView = Bundle.main.loadNibNamed("IPAddressView", owner: self, options: [:])?.first as? IPAddressView
        UIApplication.shared.keyWindow?.addSubview(ipAddressView!)
        ipAddressView?.isDismissable = true
        ipAddressView?.enableAutoLayout()
        ipAddressView?.leadingMargin(pixels: 0)
        ipAddressView?.trailingMargin(pixels: 0)
        ipAddressView?.topMargin(pixels: 0)
        ipAddressView?.bottomMargin(pixels: 0)
        
        ipAddressView?.onSuccess {
            let app_del = UIApplication.shared.delegate as! AppDelegate
            app_del.setDashboardAsRoot()
        }
    }
    
    func refreshUserLogin()
    {
        let param = [
            "mobile":Preferences.getUserMobileNo() as AnyObject,
            "imei":VIEWMANAGER.getDeviceID()
            ] as [String : AnyObject]
//        let param = [
//            "mobile":"8600666026",
//            "imei":"CB6892F2-0DB4-4901-8489-B971956F1E83"
//            ] as [String : AnyObject]

        RemoteAPI().callPOSTApi(apiName: LOGIN_API, params: param, completion: { (responseData) in
            let success = responseData as! Bool
            if success{
            }
            else{
                Preferences.saveUserId(userId: "")
                let app_del = UIApplication.shared.delegate as! AppDelegate
                app_del.setLoginAsRoot()
                UIApplication.shared.keyWindow?.makeToast("You are not authorize user. Please contact admin")
            }
        }) { (errMsg) in
//            self.view.makeToast(errMsg)
        }
    }
    
    func showLogoutAlert() {
        let otherAlert = UIAlertController(title: "Logout", message: "Do you want to logout?", preferredStyle: UIAlertControllerStyle.alert)

            let printSomething = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { _ in
                Preferences.saveUserId(userId: "")
                let app_del = UIApplication.shared.delegate as! AppDelegate
                app_del.setLoginAsRoot()
            }

            let dismiss = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil)

            // relate actions to controllers
            otherAlert.addAction(printSomething)
            otherAlert.addAction(dismiss)

            self.present(otherAlert, animated: true, completion: nil)
    }
}
