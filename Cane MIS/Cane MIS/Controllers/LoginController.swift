//
//  LoginController.swift
//  Cane MIS
//
//  Created by iLoma on 11/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import UIKit

class LoginController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var txtFieldEnterMoNumber: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    var gradientLayer = CAGradientLayer()
    var currentUpperColor = UIColor(red:0.11, green:0.62, blue:0.85, alpha:1.0)
    var currentMiddleColor = UIColor(red:0.03, green:0.15, blue:0.46, alpha:1.0)
    var currentLowerColor = UIColor(red:0.01, green:0.01, blue:0.28, alpha:1.0)
    @IBOutlet weak var scrollLogin: UIScrollView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground(lowerColor: currentLowerColor, midleColor: currentMiddleColor, upperColor: currentUpperColor)
        //textField
        txtFieldEnterMoNumber.delegate=self;
        txtFieldEnterMoNumber.layer.cornerRadius = 25.0
        txtFieldEnterMoNumber.layer.borderWidth = 2.0
        txtFieldEnterMoNumber.layer.borderColor = UIColor.white.cgColor
        //login button
        btnLogin.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.85, alpha:1.0)
        btnLogin.layer.cornerRadius = 25.0
        
        scrollLogin.contentSize.height = 1.0
        
//        if Preferences.isIMEIShown() == false {
//            showMessagePopup()
//            //showIPAddressDialog()
//        }
        
        if (Preferences.getIPAddress() == nil) {
            showIPAddressDialog()
        }
        
       // txtFieldEnterMoNumber.text = "8600666026"
    }
    
    

    func setGradientBackground(lowerColor: UIColor,midleColor: UIColor, upperColor: UIColor)
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [lowerColor.cgColor, upperColor.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        if validate() == true {
            if Preferences.getIPAddress() != nil{
                 loginService()
            }
           
        }
    }
    
    func validate() -> Bool
    {
        if txtFieldEnterMoNumber.text!.count == 0 {
            self.view.makeToast("Mobile number is mandatory.")
            return false
        }
        if txtFieldEnterMoNumber.text!.count < 10 {
            self.view.makeToast("Enter valid mobile number.")
            return false
        }
        return true
    }
    
    func loginService() {
        
        let param = [
                "mobile":txtFieldEnterMoNumber.text!,
                "imei":VIEWMANAGER.getDeviceID()
        ] as [String : AnyObject]

        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: LOGIN_API, params: param, completion:   { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            Preferences.saveUserMobileNo(userId: self.txtFieldEnterMoNumber.text!)
            let success = responseData as! Bool
            if success{
                let app_del = UIApplication.shared.delegate as! AppDelegate
                app_del.setDashboardAsRoot()
            }
            else{
                self.view.makeToast("Enter valid number registered on ERP")
            }
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            self.view.makeToast(errMsg)
        }
    }

    func showMessagePopup()
    {
        let alert = CustomAlertView(frame: self.view.bounds, strMessage: "Before using this application on this device, enter the following number in ERP system\n\n\(VIEWMANAGER.getDeviceID())")
        self.view.addSubview(alert)
    }
    
    func showIPAddressDialog(){
        let ipAddressView = Bundle.main.loadNibNamed("IPAddressView", owner: self, options: [:])?.first as? IPAddressView
        UIApplication.shared.keyWindow?.addSubview(ipAddressView!)
        ipAddressView?.isDismissable = false
        ipAddressView?.enableAutoLayout()
        ipAddressView?.leadingMargin(pixels: 0)
        ipAddressView?.trailingMargin(pixels: 0)
        ipAddressView?.topMargin(pixels: 0)
        ipAddressView?.bottomMargin(pixels: 0)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= 10

    }
}
