//
//  CaneCrushingController.swift
//  Cane MIS
//
//  Created by iLoma on 12/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import UIKit

class CaneCrushingController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
 
    @IBOutlet weak  var lblDate: UILabel!

    @IBOutlet weak var btnSelectDate: UIButton!
    @IBOutlet weak var btnDay: UIButton!
    @IBOutlet weak var btnMonth: UIButton!
    @IBOutlet weak var btnYear: UIButton!
    var arrTotal = NSMutableArray()
    var arrUptoDateTotal = NSMutableArray()

    var arrData = NSArray()
    var timer = Timer()
    var arrRow1 = ["Shift","04-12(A)","12-08(B)","08-04(C)","Total","Upto Date Total"]
    var arrRow2 = ["Shift","Crushing (MT)","Suger (QTL)","Molasses (MT)","R.S./S.D.S (LTR)","Ethenol (LTR)"]
    @IBOutlet weak var collectionHtConstr: NSLayoutConstraint!
    
    
    @IBOutlet weak var collectionCrushingProduction: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSelectDate.layer.cornerRadius = 25
        btnDay.layer.cornerRadius = 25
        btnMonth.layer.cornerRadius = 25
        btnYear.layer.cornerRadius = 25
        
        self.setDateAndTime()
        
        setupDayMonthYear(from: getTodaysDate())
        
        if VIEWMANAGER.seasonID == nil {
            VIEWMANAGER.getMISSeasonID(completion: { (status) in
                if status{
                    self.getShiftWiseData(date: self.getTodaysDate(),season: VIEWMANAGER.seasonID)
                }
            })
        }
        else{
              self.getShiftWiseData(date: self.getTodaysDate(),season: VIEWMANAGER.seasonID)
        }
    }
    
    func getShiftWiseData(date:String,season:String)
    {
        let param = [
            "date":date,
            "season":season
            ] as [String : AnyObject]
        
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_SHIFTWISE_DATA_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrData = responseData as! NSArray
            self.arrTotal.removeAllObjects()
            self.arrUptoDateTotal.removeAllObjects()
            for j in 0...4
            {
                var total : Float = 0
                var uptoTotal : Float = 0
                for i in 0...2 {
                    let model  = self.arrData[i] as! ShiftWiseModel
                    if j == 0{
                        total = total + Float(model.cRUSNETWT)!
                        uptoTotal = uptoTotal + Float(model.aSCRUSNETWT)!
                    }
                    else if j == 1{
                        total = total + Float(model.sUGARNETWT)!
                        uptoTotal = uptoTotal + Float(model.aSSUGARNETWT)!
                    }
                    else if j == 2{
                        total = total + Float(model.mOLANETWT)!
                        uptoTotal = uptoTotal + Float(model.aSMOLANETWT)!
                    }
                    else if j == 3{
                        total = total + Float(model.aLCOHOLNETWT)!
                        uptoTotal = uptoTotal + Float(model.aSALCOHOLNETWT)!
                    }
                    else {
                        total = total + Float(model.cOGENNETWT)!
                        uptoTotal = uptoTotal + Float(model.aSCOGENNETWT)!
                    }
                }
                self.arrTotal.add(total);
                self.arrUptoDateTotal.add(uptoTotal)
            }
            
            self.collectionCrushingProduction.reloadData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
             self.view.makeToast(errMsg)
        }
    }
    
    func setDateAndTime() {

        lblDate.text = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .medium)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
    }
    @objc func tick() {
        lblDate.text = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .medium)
    }
    
    func setupShadow(_ view: UIView?) {
        view?.layer.masksToBounds = false
        view?.layer.shadowColor = UIColor.black.cgColor
        view?.layer.shadowOffset = CGSize(width: 2, height: 2)
        view?.layer.borderWidth = 0.5
        view?.layer.borderColor = UIColor.lightGray.cgColor
        view?.layer.shadowOpacity = 0.2
    }
   
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "crushingCell", for: indexPath) as! CaneCrushingCollectionCell
        
        cell.lblTitle.textColor = .black
        cell.lblTitle.adjustsFontSizeToFitWidth = true
        cell.lblTitle.font = UIFont.systemFont(ofSize: 14)
        
        if indexPath.section == 0 {
            cell.lblTitle.textAlignment = .left
            cell.lblTitle.text = arrRow2[indexPath.row]
            cell.backgroundColor = .gray
            cell.lblTitle.font = UIFont.boldSystemFont(ofSize: 14)
        }
        else{
            if indexPath.section == 1{
                putData(indexPath: indexPath,cell: cell,shiftCode: "A")
            }
            else if indexPath.section == 2{
                putData(indexPath: indexPath,cell: cell,shiftCode: "B")
            }
            else if indexPath.section == 3{
                putData(indexPath: indexPath,cell: cell,shiftCode: "C")
            }
            else if indexPath.section == 4{
                cell.backgroundColor = .darkGray
                cell.lblTitle.textColor = .white
                cell.lblTitle.textAlignment = .right
                if arrTotal.count == 5 && indexPath.row > 0{
                    cell.lblTitle.text = "\(arrTotal[indexPath.row - 1])"
                }
                else if indexPath.row == 0{
                    cell.lblTitle.font = UIFont.boldSystemFont(ofSize: 15)
                    cell.lblTitle.text = arrRow1[indexPath.section]
                }
                else{
                    cell.lblTitle.text = "0"
                }
            }
            else if indexPath.section == 5{
                cell.lblTitle.textAlignment = .right
                cell.backgroundColor = .darkGray
                cell.lblTitle.textColor = .white
                if arrUptoDateTotal.count == 5 && indexPath.row > 0{
                    cell.lblTitle.font = UIFont.systemFont(ofSize: 14)
                    cell.lblTitle.text = NSString(format: "%.0f", arrUptoDateTotal[indexPath.row - 1] as! Float) as String
                }
                else if indexPath.row == 0{
                    cell.lblTitle.font = UIFont.boldSystemFont(ofSize: 15)
                    cell.lblTitle.text = arrRow1[indexPath.section]
                }
                else{
                    cell.lblTitle.text = "0"
                }
            }
            else{
                 cell.lblTitle.text = "0"
            }
        }
        return cell
    }
    
    
    func putData(indexPath:IndexPath,cell:CaneCrushingCollectionCell,shiftCode:String)
    {
        if indexPath.row != 0 {
            cell.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.78, alpha: 1.0)
            let filteredArray = arrData.filter() { ($0 as! ShiftWiseModel).sHIFTCODE == shiftCode }
            cell.lblTitle.textAlignment = .right
            if filteredArray.count > 0
            {
                let model = filteredArray[0] as! ShiftWiseModel
                if indexPath.row == 1{
                    cell.lblTitle.text = model.cRUSNETWT
                }
                else if indexPath.row == 2{
                    cell.lblTitle.text = model.sUGARNETWT
                }
                else if indexPath.row == 3{
                    cell.lblTitle.text = model.mOLANETWT
                }
                else if indexPath.row == 4{
                    cell.lblTitle.text = model.aLCOHOLNETWT
                }
                else{
                    cell.lblTitle.text = model.cOGENNETWT
                }
            }
        }
        else{
            cell.backgroundColor = .gray
            cell.lblTitle.text = arrRow1[indexPath.section]
            cell.lblTitle.textAlignment = .center
            cell.lblTitle.font = UIFont.boldSystemFont(ofSize: 14)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
       print("section - \(indexPath.section)   row - \(indexPath.row)")

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(0, 1, 0, 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }

    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectDateClicked(_ sender: UIButton) {
        showDatePicker { (date) in
            self.getShiftWiseData(date: date,season: VIEWMANAGER.seasonID != nil ? VIEWMANAGER.seasonID : "")
            self.setupDayMonthYear(from: date)
        }
    }
    
    
    func showDatePicker(completion:@escaping (String)->())
    {
        let datePicker = DatePickerView(frame: UIScreen.main.bounds)
        self.navigationController?.view.addSubview(datePicker)
        datePicker.onDoneClicked { (date) in
            completion(date)
        }
    }
    
    func getTodaysDate() -> String
    {
        let today: Date? = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MMM-yyyy"
        var dateString: String? = nil
        if let aDate = today {
            dateString = dateFormat.string(from: aDate)
        }
        return dateString!
    }
    
    func setupDayMonthYear(from:String)
    {
        let strDate = from;
        let arr = strDate.components(separatedBy: "-")
        if arr.count == 3 {
            btnDay.setTitle(arr[0], for: .normal)
            btnMonth.setTitle(arr[1], for: .normal)
            btnYear.setTitle(arr[2], for: .normal)

        }
    }
    
    @IBAction func btnHomeClicked(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
