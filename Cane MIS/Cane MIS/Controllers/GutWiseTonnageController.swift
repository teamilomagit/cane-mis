//
//  GutWiseTonnageController.swift
//  Cane MIS
//
//  Created by TIU-User on 02/12/20.
//  Copyright © 2020 iLoma. All rights reserved.
//

import UIKit

class GutWiseTonnageController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var viewDateTime: UIView!
    @IBOutlet weak var lblDate: UILabel!
        var timer = Timer()
        var arrTotal = NSMutableArray()
        var arrData = NSMutableArray()
        var selDate : String!
        
        @IBOutlet weak var btnSelectDate: UIButton!
        @IBOutlet weak var btnDay: UIButton!
        @IBOutlet weak var btnMonth: UIButton!
        @IBOutlet weak var btnYear: UIButton!

        @IBOutlet weak var tableViewGutWise: UITableView!
        var fromVillageWise = false
        var fromShiftWiseWise = false

        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            
            lblNavTitle.text = "\(fromVillageWise ? "Village" : (fromShiftWiseWise ? "Shift" : "Gut")) Wise Tonnage"
            tableViewGutWise.delegate = self
            tableViewGutWise.dataSource = self
            btnSelectDate.layer.cornerRadius = 25
            btnDay.layer.cornerRadius = 25
            btnMonth.layer.cornerRadius = 25
            btnYear.layer.cornerRadius = 25
            self.setDateAndTime()
            
            selDate = getTodaysDate()
            setupDayMonthYear(from: selDate)
            if VIEWMANAGER.seasonID == nil {
                VIEWMANAGER.getMISSeasonID(completion: { (status) in
                    if status
                    {
                        self.getGutWiseData(date: self.selDate,season: VIEWMANAGER.seasonID)
                    }
                })
            }
            else{
                self.getGutWiseData(date: self.selDate,season: VIEWMANAGER.seasonID)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10)) {
                self.getGutWiseData(date: self.selDate,season: VIEWMANAGER.seasonID != nil ? VIEWMANAGER.seasonID : "",showLoader: false)
            }

        }
        
        func getGutWiseData(date:String,season:String,showLoader:Bool = true)
        {
            let param = [
                "date":date,
                "season":season
                ] as [String : AnyObject]
            if showLoader {
                VIEWMANAGER.showActivityIndicator()
            }
            RemoteAPI().callPOSTApi(apiName: fromVillageWise ? GET_MIS_VILLAGEWISE_API : (fromShiftWiseWise ? GET_MIS_SHIFTWISE_API : GET_MIS_GUTWISE_API), params: param, completion: { (responseData) in
                VIEWMANAGER.hideActivityIndicator()
                self.arrData = responseData as! NSMutableArray
                
                var totalUptoDateTonnag : Float = 0
                var totalTodaysTonnage: Float = 0.0
                for mod in self.arrData {
                    let model = mod as! GutWiseTonnageModel
                    totalTodaysTonnage = totalTodaysTonnage + Float(self.fromShiftWiseWise ? model.nETWT : model.tODAYTONNAGE)!
                    totalUptoDateTonnag = totalUptoDateTonnag + Float(self.fromShiftWiseWise ? model.cUMULATIVEWT :model.aSONTONNAGE)!
                }
                
                let model = GutWiseTonnageModel()
                model.cODE = "Total"
                model.tODAYTONNAGE = "\(totalTodaysTonnage)"
                model.aSONTONNAGE = "\(totalUptoDateTonnag)"
                
                model.fROMTOTIME = model.cODE
                model.nETWT = model.tODAYTONNAGE
                model.cUMULATIVEWT = ""
                
                self.arrData.add(model)
                self.tableViewGutWise.reloadData()
            }) { (errMsg) in
                VIEWMANAGER.hideActivityIndicator()
                 self.view.makeToast(errMsg)
            }
        }
        
        func setDateAndTime() {
            
            lblDate.text = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .medium)
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
        }
        
        @objc func tick() {
            lblDate.text = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .medium)
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return  arrData.count
        }
      
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "GutWiseTonnageCell") as! GutWiseTonnageCell
            
            cell.lblCode.font = UIFont.boldSystemFont(ofSize: 14)

            let model = arrData[indexPath.row] as! GutWiseTonnageModel
            cell.lblCode.text = fromShiftWiseWise ? " \(model.fROMTOTIME ?? "-")" : " \(model.cODE!)"
            cell.lblGutName.text = " " + (fromShiftWiseWise ? (model.oUTSHIFTCODE ?? "") : (model.nAME ?? ""))
            cell.lblTodayTonnage.text = (fromShiftWiseWise ? (model.nETWT ?? "") : model.tODAYTONNAGE) + " "
            cell.lblUptoDateTonnage.text = " " + (fromShiftWiseWise ? (model.cUMULATIVEWT ?? "") : model.aSONTONNAGE) + " "
            
            cell.lblCode.textAlignment = .left
            cell.lblGutName.textAlignment = fromShiftWiseWise ? .center: .left
            cell.lblTodayTonnage.textAlignment = .right
            cell.lblUptoDateTonnage.textAlignment = .right

            cell.lblCode.font = UIFont.boldSystemFont(ofSize: 15)
            cell.lblGutName.font = UIFont.systemFont(ofSize: 15)
            cell.lblTodayTonnage.font = UIFont.systemFont(ofSize: 15)
            cell.lblUptoDateTonnage.font = UIFont.systemFont(ofSize: 15)

            if indexPath.row == arrData.count - 1 {
                cell.lblCode.backgroundColor = UIColor.clear
                cell.baseView.backgroundColor = UIColor.darkGray

                cell.lblCode.textColor = UIColor.white
                cell.lblGutName.textColor = UIColor.white
                cell.lblTodayTonnage.textColor = UIColor.white
                cell.lblUptoDateTonnage.textColor = UIColor.white
            }
            else{
                cell.lblCode.backgroundColor = UIColor.lightGray
                cell.baseView.backgroundColor = UIColor.controllerBGColor

                cell.lblCode.textColor = UIColor.black
                cell.lblGutName.textColor = UIColor.black
                cell.lblTodayTonnage.textColor = UIColor.black
                cell.lblUptoDateTonnage.textColor = UIColor.black
            }
            
            return cell
        }
      
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 40
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
        {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
            
            let baseView = UIView()
            baseView.backgroundColor = UIColor.lightGray
            headerView.addSubview(baseView)
            baseView.enableAutoLayout()
            baseView.leadingMargin(pixels: 5)
            baseView.topMargin(pixels: 0)
            baseView.bottomMargin(pixels: 0)
            baseView.trailingMargin(pixels: 5)
            
            let lblTitle1 = UILabel()
            lblTitle1.font = UIFont.boldSystemFont(ofSize: 15)
            lblTitle1.text = fromShiftWiseWise ? " Time" : " Code"
            lblTitle1.numberOfLines = 0;
            lblTitle1.textAlignment = .left
            baseView.addSubview(lblTitle1)
            
            lblTitle1.enableAutoLayout()
            lblTitle1.leadingMargin(pixels: 2)
            lblTitle1.topMargin(pixels: 0)
            lblTitle1.bottomMargin(pixels: 0)
            
            let lblTitle2 = UILabel()
            lblTitle2.font = UIFont.boldSystemFont(ofSize: 15)
            lblTitle2.text = "\(fromVillageWise ? " Village Name" : (fromShiftWiseWise ? " Shift" : " Gut Name"))"
            lblTitle2.numberOfLines = 0;
            lblTitle2.textAlignment = fromShiftWiseWise ? .center: .left
            baseView.addSubview(lblTitle2)
            
            lblTitle2.enableAutoLayout()
            lblTitle2.addToRightToView(view: lblTitle1, pixels: 0)
            lblTitle2.topMargin(pixels: 0)
            lblTitle2.bottomMargin(pixels: 0)
            lblTitle2.equalWidthToView(toView: lblTitle1,multiplier: 1.51)
            
            let lblTitle3 = UILabel()
            lblTitle3.font = UIFont.boldSystemFont(ofSize: 15)
            lblTitle3.text = fromShiftWiseWise ? "Tonnage" : "Today\nTonnage "
            lblTitle3.numberOfLines = 0;
            lblTitle3.textAlignment = .right
            baseView.addSubview(lblTitle3)
            
            lblTitle3.enableAutoLayout()
            lblTitle3.addToRightToView(view: lblTitle2, pixels: 2)
            lblTitle3.topMargin(pixels: 0)
            lblTitle3.bottomMargin(pixels: 0)
            lblTitle3.equalWidthToView(toView: lblTitle2)
            
            let lblTitle4 = UILabel()
            lblTitle4.font = UIFont.boldSystemFont(ofSize: 15)
            lblTitle4.text = fromShiftWiseWise ? "Total\nTonnage" : "Upto Date\nTonnage "
            lblTitle4.numberOfLines = 0;
            lblTitle4.textAlignment = .right
            baseView.addSubview(lblTitle4)
            
            lblTitle4.enableAutoLayout()
            lblTitle4.addToRightToView(view: lblTitle3, pixels: 2)
            lblTitle4.topMargin(pixels: 0)
            lblTitle4.bottomMargin(pixels: 0)
            lblTitle4.trailingMargin(pixels: 2)
            lblTitle4.equalWidthToView(toView: lblTitle3)
            
            let separator1 = UIView()
            separator1.backgroundColor = .white
            baseView.addSubview(separator1)
            separator1.enableAutoLayout()
            separator1.addToRightToView(view: lblTitle1, pixels: 0)
            separator1.topMargin(pixels: 0)
            separator1.bottomMargin(pixels: 0)
            separator1.fixedWidth(pixels: 1)
            
            let separator2 = UIView()
            separator2.backgroundColor = .white
            baseView.addSubview(separator2)
            separator2.enableAutoLayout()
            separator2.addToRightToView(view: lblTitle2, pixels: 1)
            separator2.topMargin(pixels: 0)
            separator2.bottomMargin(pixels: 0)
            separator2.fixedWidth(pixels: 1)
            
            let separator = UIView()
            separator.backgroundColor = .white
            baseView.addSubview(separator)
            separator.enableAutoLayout()
            separator.addToRightToView(view: lblTitle3, pixels: 0)
            separator.bottomMargin(pixels: 0)
            separator.topMargin(pixels: 0)
            separator.fixedWidth(pixels: 1)
         //   separator.leadingMargin(pixels: 0)
           // separator.trailingMargin(pixels: 0)
            //separator.fixedHeight(pixels: 1)
            return headerView
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 50
        }
        @IBAction func btnBackClicked(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)

        }
    
        @IBAction func btnSelectDateClicked(_ sender: UIButton) {
            showDatePicker { (date) in
                self.getGutWiseData(date: date,season: VIEWMANAGER.seasonID != nil ? VIEWMANAGER.seasonID : "")
                self.setupDayMonthYear(from: date)
            }
        }
    
    
        func showDatePicker(completion:@escaping (String)->()) {
            let datePicker = DatePickerView(frame: UIScreen.main.bounds)
            self.navigationController?.view.addSubview(datePicker)
            datePicker.onDoneClicked { (date) in
                completion(date)
            }
        }

        func setupDayMonthYear(from:String) {
            let strDate = from
            let arr = strDate.components(separatedBy: "-")
            if arr.count == 3 {
                btnDay.setTitle(arr[0], for: .normal)
                btnMonth.setTitle(arr[1], for: .normal)
                btnYear.setTitle(arr[2], for: .normal)
            }
        }
    
        func getTodaysDate() -> String
        {
            let today: Date? = Date()
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "dd-MMM-yyyy"
            var dateString: String? = nil
            if let aDate = today {
                dateString = dateFormat.string(from: aDate)
            }
            return dateString!
        }
       
        @IBAction func btnHomeClicked(_ sender: Any) {
            self.navigationController?.popToRootViewController(animated: true)
        }
}
